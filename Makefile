.PHONY: go, main

name="Quantize"
package_contents="Debug doc doxyfile main.cpp Makefile modules README.md"

graph=callgraph.dot

go:
	(cd Debug ; ./go)
main:
	(cd Debug ; make)
dox:
	doxygen doxyfile
show-dox:
	xdg-open html/index.html
pack:
	zip -r "$(name).zip" "$(package_contents)"
