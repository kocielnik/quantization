/*
 * main.cpp
 *
 *  Created on: 20 juin 2017
 *      Author: patryk
 */

#include <iostream>
#include <stdlib.h>

#include "modules/utest/utest.hpp"
#include "modules/verify.hpp"

// Logger
// https://stackoverflow.com/questions/6168107/how-to-implement-a-good-debug-logging-feature-in-a-project?rq=1

int main(int argc, char** argv) {

//	run_unit_tests();

	test_interface(argc, argv);

	return 0;
}

