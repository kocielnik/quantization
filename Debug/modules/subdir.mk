################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../modules/ScalarQuantizer.cpp \
../modules/ssim.cpp \
../modules/verify.cpp 

OBJS += \
./modules/ScalarQuantizer.o \
./modules/ssim.o \
./modules/verify.o 

CPP_DEPS += \
./modules/ScalarQuantizer.d \
./modules/ssim.d \
./modules/verify.d 


# Each subdirectory must supply rules for building sources it contributes
modules/%.o: ../modules/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	clang++ -std=c++1y -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


