#ifndef LMQ_HPP
#define LMQ_HPP

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <math.h>
#include <cassert>
#include <algorithm>
#include <fstream>

#include "verify.hpp"
#include "logger.hpp"

//#include <opencv/imgproc.h>

class ScalarQuantizer {

public:
	ScalarQuantizer();
	~ScalarQuantizer();
	// Returns SSIM
	enum bin_placement {REGULAR, RANDOM, WEIGHTED /* K-means */, ADAPTIVE /* Lloyd */};
	float quantize(const std::string file, const int bits, const enum bin_placement bp = bin_placement::REGULAR, loglevel_e logl = logINFO);

private:
	// Testable functions:
	int test_which_interval();
	void set_log_level(loglevel_e logl) {
		loglevel = logl;
	}
	int save_pgm(const std::string file, cv::Mat payload);
	float mean(const std::vector<int> values, float lbound = -INFINITY, float ubound = INFINITY);
	float wmean(const std::vector<int> values, float lbound = -INFINITY, float ubound = INFINITY);
	std::vector<float> k_means(const std::vector<int> pdf, const std::vector<float> thresholds, std::vector<float>& centers);
	std::vector<int> process(const std::vector<int> data, const int count, std::vector<float>& thresholds, std::vector<float>& centers, bin_placement bp = bin_placement::REGULAR);
	std::string print_pub(const std::vector<float> t);
	std::string print_pub(const std::vector<int> t);

	friend int run_unit_tests();

//	ScalarQuantizer(const std::vector<int> data, const int count);
	//std::vector<int> process(const std::vector<int> data, const int count);
	int compare(const std::string file_name1, const std::string file_name2);
	void logTest(void);
//	enum bin_placement b;
	// define and turn off for the rest of the test suite
	loglevel_e loglevel;

	std::pair<std::vector<float>, std::vector<float> >
			adapt(const std::vector<int> data,
					const int count,
					const std::map<int, int> hist,
					std::vector<float> thresholds,
					std::vector<float> centers);

//	std::pair<std::vector<float>, std::vector<float> >
//			initialize(const std::vector<int> data, std::map<int, int> hist, const int count);

	std::map<int, int>
			bin_counts(const std::vector<int> in);

	std::vector<int> mtv(const std::map<int, int> hist);

	float getQuantizedValue(
			const float key,
			const std::vector<float> thresholds,
			const std::vector<float> centers);

	float getMSE(const std::vector<int>& data,
			const std::vector<float>& thresholds,
			const std::vector<float>& centers);

	std::vector<float> recalculate_centers(const std::vector<int> pdf, const std::vector<float> thresholds, const std::vector<float> centers);

	std::vector<float> recalculate_thresholds(std::vector<float> thresholds, const std::vector<float> centers);

//	std::vector<int> quantize(const std::vector<int> data, const int count);

	std::vector<int> analyze_and_quantize(const std::vector<int> data, const int count, std::vector<float>& thresholds, std::vector<float>& centers, bin_placement bp = bin_placement::REGULAR);

	bool found(const std::vector<float>& vec, const int& num);

	int which_interval(const std::vector<float> thresholds, const float& value);

	// Smallest In Map
	int sim(const std::map<int, int> m);

	// Greatest In Map
	int gim(const std::map<int, int> m);

	std::vector<int> print_pdf(const std::vector<int> t);

	template <typename T>
			std::string print( T t );
//	int min(const std::vector<int> v);
//
//	int max(const std::vector<int> v);

	std::vector<float> slice(
			const int maxval /* Maximum input value */,
			const int count /* Number of output thresholds */);

	std::vector<float> place_centers(const std::vector<float> thresholds);

	bool find(const std::vector<float> vec, const int val);

	std::vector<int> pdf(const std::vector<int> data);

	std::pair<std::vector<float>, std::vector<float>> randomize_thresholds(const std::vector<int> data, const std::map<int, int> hist, const int count);
};

namespace sq {

float q(const std::string file, const int bits, const enum ScalarQuantizer::bin_placement bp = ScalarQuantizer::bin_placement::REGULAR, loglevel_e logl = logINFO);

}

#endif /* LMQ_HPP */
