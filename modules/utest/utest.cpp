/*
 * utest.cpp
 *
 *  Created on: 10 Jan 2017
 *      Author: patryk
 */

#include "utest.hpp"

#define BITS 4

void test_setup() {
}

void test_teardown() {
}

#ifdef TEST_INTERNAL_MECHANISMS
MU_TEST(test_mean) {
	ScalarQuantizer sq;

	std::vector<int> pdf;

//	val    0  1  2  3  4  5  6  7  8
	pdf = {0, 0, 0, 1, 1, 1, 0, 0, 0};

	float expect = 1;

	float res;

	res = sq.mean(pdf);

	mu_assert(res == expect, "Mean value should be calculated properly.");
}

MU_TEST(test_weighted_mean) {
	ScalarQuantizer sq;

	std::vector<int> pdf;

//	val    0, 1  2  3  4  5  6  7  8
	pdf = {0, 0, 0, 1, 1, 1, 0, 0, 0};

	float expect = 4;

	float res;

	res = sq.wmean(pdf);

	mu_assert(res == expect, "Weighted mean value should be calculated properly.");
}

MU_TEST(test_k_means) {

	ScalarQuantizer sq;

	std::vector<int> pdf;

//	val    0, 1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18
	pdf = {0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1,  1,  1,  1,  0,  0,  0,  0,  0};

	// expected results
	std::vector<float> exp;

	exp = {4, 11.5};

	std::vector<float> out;
	std::vector<float> thresholds, centers;

	thresholds = {0, 9, 18};

	sq.set_log_level(logDEBUG);

	out = sq.k_means(pdf, thresholds, centers);

//	std::cout << "Determined centers:\n";

//	sq.print_pub(out);

//	sq.k_means()

	mu_assert( out == exp, "K-means should have been found properly.");
}

// Test if first transaction is correctly parsed into the output vector.
MU_TEST(test_interval_assignment) {

	ScalarQuantizer sq;

	mu_assert( sq.test_which_interval() == 0, "Intervals should be properly determined.");
}

MU_TEST(test_save_pgm) {
	ScalarQuantizer sq;

	cv::Mat out( 2, 2, CV_32FC1);
	out = (cv::Mat_<unsigned char>(2, 2) << 0, 1 * 64, 2 * 64, 3 * 64);

	cv::Mat in;

	std::string file = "test.pgm";

	sq.save_pgm(file, out);

	in = cv::imread(file, 0);

	bool result = true;

	int a, b;

	for (int i = 0; i < out.size().width; i++) {
		for (int j = 0; j < out.size().height; j++) {
			a = out.at<unsigned char>(i, j);// std::cout << a <<std::endl;
			b = in.at<unsigned char>(i, j);// std::cout << b <<std::endl;
			if (a != b) {
				result = false;
				break;
			}
		}
	}

	remove( file.c_str() );

	// Print matrices
//	std::cout << "out = "<< std::endl << " "  << out << std::endl << std::endl;
//	std::cout << "in = "<< std::endl << " "  << in << std::endl << std::endl;

	mu_assert(result, "PGM image should be saved properly.");
}
#endif

MU_TEST(test_regular_quantization) {

	ScalarQuantizer sq;

	std::string infile = "./Debug/t/obrazy/lena.pgm";
	size_t bits = BITS;

	float ssim = -1;

	ssim = sq.quantize(infile, bits, ScalarQuantizer::bin_placement::REGULAR);

	mu_assert( ssim > 0.85, "Regularly quantized image should be\nvisually close to the original image\n by more than 85%.");
}

MU_TEST(test_random_quantization) {

	ScalarQuantizer sq;

	std::string infile = "./Debug/t/obrazy/lena.pgm";
	size_t bits = BITS;

	float ssim = -1;

	ssim = sq.quantize(infile, bits, ScalarQuantizer::bin_placement::RANDOM);

	mu_assert( ssim > 0.60, "Image similarity should exceed 60%.");
}

MU_TEST(test_weighted_quantization) {

	ScalarQuantizer sq;

	std::string infile = "./Debug/t/obrazy/lena.pgm";
	size_t bits = BITS;

	float ssim = -1;

	ssim = sq.quantize(infile, bits, ScalarQuantizer::bin_placement::WEIGHTED);

	mu_assert( ssim > 0.85, "Image similarity should exceed 90%.");
}

//MU_TEST(test_write_pgm) {
//
//	ScalarQuantizer sq;
//
//	std::string infile = "./Debug/t/obrazy/lena.pgm";
//	size_t bits = 6;
//
//	float ssim = -1;
//
//	ssim = sq.q(infile, bits, ScalarQuantizer::bin_placement::WEIGHTED);
//
//	mu_assert( ssim > 0.85, "Image similarity should exceed 90%.");
//}

MU_TEST(test_adaptive_quantization) {

	ScalarQuantizer sq;

	std::string infile = "./Debug/t/obrazy/lena.pgm";
	size_t bits = BITS;

	float ssim = -1;

	ssim = sq.quantize(infile, bits, ScalarQuantizer::bin_placement::ADAPTIVE);

	mu_assert( ssim > 0.87, "Image similarity should exceed 87%.");
}

#ifdef TEST_INTERNAL_MECHANISMS
MU_TEST_SUITE(test_internal_mechanisms) {
	MU_SUITE_CONFIGURE(&test_setup, &test_teardown);

	MU_RUN_TEST(test_mean);
	MU_RUN_TEST(test_weighted_mean);
	MU_RUN_TEST(test_k_means);
	MU_RUN_TEST(test_interval_assignment);
	MU_RUN_TEST(test_save_pgm);
}
#endif

MU_TEST_SUITE(test_quantizer) {
	MU_SUITE_CONFIGURE(&test_setup, &test_teardown);

	MU_RUN_TEST(test_regular_quantization);

	MU_RUN_TEST(test_random_quantization);

	MU_RUN_TEST(test_weighted_quantization);

	MU_RUN_TEST(test_adaptive_quantization);
}

int run_unit_tests() {

#ifdef TEST_INTERNAL_MECHANISMS
	MU_RUN_SUITE(test_internal_mechanisms);
#endif

	MU_RUN_SUITE(test_quantizer);

	MU_REPORT();
	return 0;
}

