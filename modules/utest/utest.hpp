/*
 * utest.hpp
 *
 *  Created on: 9 Jan 2017
 *      Author: patryk
 */

#include <stdio.h>
#include <cstdio>

#include "minunit.h"
#include "../ScalarQuantizer.hpp"
#include "../logger.hpp"

#ifndef SRC_TEST_HPP_
#define SRC_TEST_HPP_

int run_unit_tests();

#endif /* SRC_TEST_HPP_ */
