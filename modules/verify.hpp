/*
 * verify.hpp
 *
 *  Created on: 22 juil. 2017
 *      Author: patryk
 */

#ifndef MODULES_VERIFY_HPP_
#define MODULES_VERIFY_HPP_

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
//#include <opencv2/imgproc/imgproc.hpp>

#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "ssim.hpp"
#include "ScalarQuantizer.hpp"

int test_opencv( int argc, char** argv );
int test_ext(int argc, char** argv);
int test_unit();
float process_run(const std::string file, const int bits);
int test_fio(int argc, char**argv);
void print_help(int argc, char** argv);
int test_interface(int argc, char** argv);


#endif /* MODULES_VERIFY_HPP_ */
