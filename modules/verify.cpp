/*
 * verify.cpp
 *
 *  Created on: 22 juil. 2017
 *      Author: patryk
 */

#include "verify.hpp"

int test_opencv( int argc, char** argv )
{
  cv::Mat src = cv::imread( argv[1], 1 );
  cv::Mat samples(src.rows * src.cols, 3, CV_32F);
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ )
      for( int z = 0; z < 3; z++)
        samples.at<float>(y + x*src.rows, z) = src.at<cv::Vec3b>(y,x)[z];

  int clusterCount;

  if (argc > 2)
	  clusterCount = std::stoi(argv[2]);
  else {
	  std::cout << "No output bit depth given, assuming 6 bits\n";
	  clusterCount = 6;
  }

  // Convert depth to bits
  clusterCount = (int) pow(2, clusterCount);

  cv::Mat labels;
  int attempts = 5;
  cv::Mat centers;
  cv::kmeans(samples, clusterCount, labels, cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001), attempts, cv::KMEANS_PP_CENTERS, centers );

  cv::Mat new_image( src.size(), src.type() );
  for( int y = 0; y < src.rows; y++ )
    for( int x = 0; x < src.cols; x++ )
    {
      int cluster_idx = labels.at<int>(y + x*src.rows,0);
      new_image.at<cv::Vec3b>(y,x)[0] = centers.at<float>(cluster_idx, 0);
      new_image.at<cv::Vec3b>(y,x)[1] = centers.at<float>(cluster_idx, 1);
      new_image.at<cv::Vec3b>(y,x)[2] = centers.at<float>(cluster_idx, 2);
    }

//  imshow( "clustered image", new_image );
//  waitKey( 0 );

  std::string new_name = argv[1];
  std::string ext = new_name.substr(new_name.size() - 3, 3);
  //std::cout << "Extension: " << ext << std::endl;
  new_name = new_name.substr(0, new_name.size() - 4);
  new_name = new_name + "_cvkm" + "_" + std::to_string(clusterCount) + "b_" + "." + ext;
  std::cout << "Output file:" << " " << new_name << std::endl;

  cv::imwrite(new_name, new_image);

  return 0;
}

int test_ext(int argc, char** argv) {

	cv::Mat image;

	image = cv::imread( argv[1], 1 );

	if (argc != 2 || ! image.data ) {
		std::cout << "No image data\n";
		return 1;
	}

	cv::namedWindow("Image display", CV_WINDOW_AUTOSIZE);

	imshow( "Image display", image );

	cv::waitKey(0);

	return 0;
}

#ifdef TEST_INTERNAL_MECHANISMS
int test_unit() {

	std::vector<int> in, out;
	std::vector<float> centers, thresholds;

	in = {1, 2, 49, 54, 55, 50, 34, 14, 9};

	ScalarQuantizer sq;

	out = sq.process(in, 3, thresholds, centers);

	std::cout << "Quantized output:\n";
	sq.print_pub(out);
	std::cout << "Thresholds:\n";
	sq.print_pub(thresholds);
	std::cout << "Centers:\n";
	sq.print_pub(centers);

	if (out.size() == in.size())
		return 0;
	else
		return 1;
}
#endif

#ifdef TEST_INTERNAL_MECHANISMS
float process_run(const std::string file, const int bits) {
	std::vector<int> in, out;
	std::vector<float> centers, thresholds;
	cv::Mat image;

	ScalarQuantizer sq;

	int clusterCount;

	// Convert bits to clusters
	clusterCount = (int) pow(2, bits);

	std::string new_name = file;
	std::string ext = new_name.substr(new_name.size() - 3, 3);
	//std::cout << "Extension: " << ext << std::endl;
	new_name = new_name.substr(0, new_name.size() - 4);
	new_name = new_name + "_lmq" + "_" + std::to_string(bits) + "b_" + "." + ext;
	std::cout << "Output file:" << " " << new_name << std::endl;

	image = cv::imread(file);

	in.assign(image.datastart, image.dataend);

	out = sq.process(in, clusterCount, thresholds, centers);

//	std::cout << "Quantized output:\n";
//	sq.print_pub(out);
	std::cout << "Quantization complete.\n";
	std::cout << "Thresholds:\n";
	sq.print_pub(thresholds);
	std::cout << "Centers:\n";
	sq.print_pub(centers);

	cv::Mat dest(image.rows,image.cols,image.type());

	std::vector<unsigned char> out_char;

	for (std::vector<int>::const_iterator it = out.begin(); it != out.end(); it++)
		out_char.push_back(*it);

	dest.data = out_char.data();

	cv::imwrite(new_name, dest);

	float ssim;

	ssim = SSIM::ssim(image, dest);

	std::cout << "SSIM: " << SSIM::ssim(image, dest) << std::endl;

	return ssim;
}
#endif

// Test file I/O
int test_fio(int argc, char**argv) {
	std::vector<int> in, out;
	std::vector<float> centers, thresholds;
	cv::Mat image;

	ScalarQuantizer sq;

	float ssim;

	int bits;
	ScalarQuantizer::bin_placement bp = ScalarQuantizer::bin_placement::REGULAR;
	loglevel_e logl = logINFO;

	std::string infile;
	if (argc > 1) ( infile = argv[1] );

	if (argc > 2) {
	  bits = std::stoi(argv[2]);
	}
	else {
		std::cout << "Not enough arguments. Exiting.\n";
		exit(1);
	}

	if (argc > 3) {
		bp = (ScalarQuantizer::bin_placement) std::stoi(argv[3]);
	}

	if (argc > 4)
		logl = static_cast<loglevel_e>(std::stoi(argv[4]));


//	else infile = "./Debug/t/obrazy/lena.pgm";

//	process_run(infile, bits);
	ssim = sq::q(infile, bits, bp, logl);

	std::cout << "SSIM: " << ssim << std::endl;

	return 0;
}

void print_help(int argc, char** argv) {
	std::cout << "Usage: " << argv[0] << " " << "<filename>" << " <bit_depth>"
			<< " [calibration_alg]" << " [verbosity]"
			<< std::endl;
	std::cout << "\nCalibration algorithms:\n"
			<< "0: regular,\n"
			<< "1: random,\n"
			<< "2: weighted (k-means),\n"
			<< "3: highly adaptive (Lloyd-Max).\n";

	std::cout << "\nUseful log verbosity levels:\n"
			<< "2 - Log_info,\n"
			<< "3 - Log_debug.\n\n";
}

//int test_quantization(std::vector<int> img) {
//
//	std::vector<int> out;
//
//	out =,
//
//	return 0;
//}

// TDD approach to codec implementation:
// Check if the compressed file has the
// desired level of similarity to the original.
int test_interface(int argc, char** argv) {

	if ( argc <2 ) print_help(argc, argv);

	if (//test_unit() != 0
			test_fio(argc, argv)
//		    || test_ext(argc, argv)
//			test_ext(argc, argv) != 0)
//			test_opencv(argc, argv) != 0
			)
		std::cout << "Test failed" << std::endl;

	// Compare two images and signal success if similar enough

    return 0;
}
