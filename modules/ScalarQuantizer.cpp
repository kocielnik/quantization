#include "ScalarQuantizer.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <algorithm>


/*
 * Quantize a file
 *
 * Parameters:
 * `file` 	- file name,
 * `bits` 	- number of bits of color depth in the output image,
 * `bp` 	- method of quantizer calibration.
 * `logl` 	- log density level
 *
 *   Available calibration methods:
 *   - regular,
 *   - random,
 *   - weighted (K-means), and
 *   - adaptive (Lloyd-Max).
 */

float sq::q(
		const std::string file, // file name
		const int bits, 		// number of bits of color depth in the output image,
		const enum ScalarQuantizer::bin_placement bp,
		loglevel_e logl) {

	ScalarQuantizer quantizer;

	float rv;

	rv = quantizer.quantize(file, bits, bp, logl);

	return rv;
}

ScalarQuantizer::ScalarQuantizer() {
//	 b = bin_placement::REGULAR;
	 loglevel = logDEBUG;

#ifdef LOG_TO_FILE
	 // Redirect messages to log file
	 std::ofstream out("2017.08.25.txt");
	 std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
	 std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
#endif
}

ScalarQuantizer::~ScalarQuantizer() {
#ifdef LOG_TO_FILE
    std::cout.rdbuf(coutbuf); //reset to standard output again
#endif
}

/**
 * Calculate the interval/range values
 * T = (R[i] + R[i-1])/2
 */
//void calculateIntervals() {
std::vector<float> ScalarQuantizer::recalculate_thresholds(std::vector<float> thresholds, const std::vector<float> centers) {

//	std::cout << __func__ << std::endl;

	for (size_t i = 1; i < (centers.size() - 1); i++)
		thresholds[i] = (centers[i] + centers[i - 1]) / 2;

	return thresholds;
}

void ScalarQuantizer::logTest(void) {
    loglevel_e loglevel_save = loglevel;

    loglevel = logDEBUG4;

    log(logINFO) << "foo " << "bar " << "baz";

    int count = 3;
    log(logDEBUG) << "A loop with "    << count << " iterations";
    for (int i = 0; i != count; ++i)
    {
        log(logDEBUG1) << "the counter i = " << i;
        log(logDEBUG2) << "the counter i = " << i;
    }

    loglevel = loglevel_save;
}

float ScalarQuantizer::quantize(const std::string file, const int bits, const enum bin_placement bp, loglevel_e logl) {

	loglevel = logl;

	std::vector<int> in, out;
	std::vector<float> centers, thresholds;
	cv::Mat image;

	int clusterCount;

	// Convert bits to clusters
	clusterCount = (int) pow(2, bits);

	std::string new_name = file;
	std::string ext = new_name.substr(new_name.size() - 3, 3);
	//std::cout << "Extension: " << ext << std::endl;
	new_name = new_name.substr(0, new_name.size() - 4);

	std::string mode;

	switch (bp) {
	case bin_placement::REGULAR:
		mode = "reg";
		break;
	case bin_placement::RANDOM:
		mode = "ran";
		break;
	case bin_placement::WEIGHTED:
		mode = "wei";
		break;
	case bin_placement::ADAPTIVE:
		mode = "ada";
		break;
	}

	new_name = new_name + "_" + mode + "_" + std::to_string(bits) + "b_" + "." + ext;
	log(logINFO) << "Output file:" << " " << new_name;// << '\n';

	// Without CV_LOAD_IMAGE_GRAYSCALE flag even a 1-channel image
	// was read into 3 equivalent channels
	image = cv::imread(file, CV_LOAD_IMAGE_GRAYSCALE);

	in.assign(image.datastart, image.dataend);

//	b = bp;

	out = process(in, clusterCount, thresholds, centers, bp);

//	std::cout << "Quantized output:\n";
//	sq.print_pub(out);
	log(logDEBUG) << "Quantization complete.\n";
	log(logDEBUG) << "Thresholds:\n";
	print_pub(thresholds);
	log(logDEBUG) << "Centers:\n";
	print_pub(centers);

	cv::Mat dest(image.rows,image.cols,image.type());

	std::vector<unsigned char> out_char;

	for (std::vector<int>::const_iterator it = out.begin(); it != out.end(); it++)
		out_char.push_back(*it);

	dest.data = out_char.data();

	std::vector<int> compression_params; // Stores the compression parameters

	// Ensure only 1 channel in image
//	cvtColor(dest,dest,CV_BGR2GRAY,1);
	CV_Assert(dest.channels() == 1);

	compression_params.push_back(CV_IMWRITE_PAM_FORMAT_GRAYSCALE); // Set to PXM compression
	compression_params.push_back(1); // Set format of PGM file to PPM (P6), original is P5.

	cv::imwrite(new_name, dest, compression_params);
//	save_pgm(new_name, dest);

	float ssim;

	ssim = SSIM::ssim(image, dest);

	log(logINFO) << "SSIM: " << SSIM::ssim(image, dest) << '\n';

	return ssim;
}

int ScalarQuantizer::compare(const std::string file_name1, const std::string file_name2) {
	bool r;

	std::ifstream f1(file_name1);
	std::ifstream f2(file_name2);

	if(std::equal(std::istreambuf_iterator<char>(f1),
				 std::istreambuf_iterator<char>(),
				 std::istreambuf_iterator<char>(f2)))
	   r = true;
	else
	   r = false;

	return r;
}

int ScalarQuantizer::save_pgm(const std::string file, cv::Mat pl) {

	cv::Mat payload;

	// To ensure continuity of payload:
	// If pl not continuous, direct data-wise copying
	// may fail across row boundaries.
	if (pl.isContinuous() == false)
		payload = pl.clone();
	else
		payload = pl;

	if (payload.isContinuous() == false)
		exit(1);

	int width = payload.size[0],
				height = payload.size[1];

	// To write the header to file:
	std::ofstream f(file, std::ios_base::out
						 |std::ios_base::binary
						 |std::ios_base::trunc
				   );

	int maxColorValue = 255;
	f << "P5\n" << width << " " << height << "\n" << maxColorValue << "\n";

	// To write the payload to the file:

	bool wanna_flush = true;

	// std::endl == "\n" + std::flush
	// we do not want std::flush here.

	// Convert cv::Mat to vector<char>
	std::vector<unsigned char> pixels;

	long int matsize;

	matsize = payload.size[0] * payload.size[1];

//	uchar *data = payload.data;

//	std::vector<char>::iterator it, end;
//	for(int i = 0; i < matsize; i++)
//	{
//    	pixels.push_back((unsigned char) data[i]);
////	    for (int j = 0; j < payload.size[1]; j++)
//	}

//	unsigned char row[width];

	unsigned char c;
	for (int i = 0; i < payload.size().width; i++) {
		for (int j = 0; j < payload.size().height; j++) {
			c = payload.at<unsigned char>(i, j);

			f.write(reinterpret_cast<const char*>(&c), sizeof(c));
		}
	}

	//f.write(reinterpret_cast<const char*>(pixels.data()), matsize);

	if( wanna_flush )
			f << std::flush;

	f.close();

//	f.write( reinterpret_cast<const char*>(pixels.data()), matsize );

	return 0;
}

//int ScalarQuantizer::max(const std::vector<int> v) {
//	std::vector<int>::const_iterator it;
//
//	it = v.begin();
//
//	int max;
//
//	max = *v.begin();
//
//	for (it = v.begin(); it != v.end(); it++)
//		if ( *it > max ) max = *it;
//
//	return max;
//}
//
//int ScalarQuantizer::min(const std::vector<int> v) {
//	std::vector<int>::const_iterator it;
//
//	it = v.begin();
//
//	int min;
//
//	min = *v.begin();
//
//	for (it = v.begin(); it != v.end(); it++)
//		if ( *it < min ) min = *it;
//
//	return min;
//}

// Calculate a mean of input values
// from interval: [lbound, ubound)
float ScalarQuantizer::mean(const std::vector<int> values, float lbound, float ubound) {
	float sum, mean;

	sum = 0;
	mean = 0;
	int no = 0;

	for (int i = 0; i < values.size(); i++) {
		if (values.at(i) >= lbound && values.at(i) < ubound && values.at(i) != 0) {
			sum += values.at(i);
			no ++;
		}
	}

	// To prevent division by zero
	if (values.size() == 0)
		exit(1);

	mean = sum / no;

	return mean;
}

// Calculate a weighted mean of input values
// from interval: [lbound, ubound)
float ScalarQuantizer::wmean(const std::vector<int> values, float lbound, float ubound) {

	float sum, mean, wsum;

	sum = 0;
	mean = 0;
	wsum = 0;

	for (int i = 0; i < values.size(); i++) {
		if (i >= lbound && i < ubound) {
				sum += i * values.at(i);
				wsum += values.at(i);
		}
	}

	// To prevent division by zero
	if (wsum == 0)
		return 0;

	mean = sum / wsum;

	return mean;
}


std::vector<float> ScalarQuantizer::k_means(const std::vector<int> pdf, const std::vector<float> thresholds, std::vector<float>& centers) {

	float center = 0;

	for (int i = 0; i < thresholds.size() - 1; i++) {

		center = wmean(pdf, thresholds[i], thresholds[i+1]);

		centers.push_back(center);
	}

	return centers;
}
/**
 * Recompute the values of R using the
 * probability values
 */
std::vector<float> ScalarQuantizer::recalculate_centers(const std::vector<int> pdf, const std::vector<float> thresholds, const std::vector<float> centers) {
//	std::cout << __func__ << std::endl;

	std::vector<float> new_centers;

	new_centers = centers;

//	long int num, den;
//	int lower = 0, higher = 0;

	for (size_t i = 0; i < centers.size(); i++) {

		new_centers.at(i) = wmean(pdf, thresholds.at(i), thresholds.at(i+1));

		// To prevent zeros trailing the center list
		if (new_centers.at(i) == 0
				&& (int) i - 2 > 0 // new_centers.at() reported i of MAXVAL when (i - 1) > 0 only was verified.
				&& i < centers.size()
				&& new_centers.at(i - 1) > new_centers.at(i)
				)
			new_centers.at(i) = pdf.size() - 1;

	}
		//upper range for lower
//		std::cout << "i = " << i << std::endl;
//		std::cout << "thresholds[i] = " << thresholds[i];
//		std::cout << "[Lower: " << lower << std::endl;
		//for (int j = 0; j < ScalarQuantizer::levels; j++)

		//lower = ceilf(ScalarQuantizer::thresholds[i]);
//		std::cout << "]Lower: " << lower << std::endl;

		//If last value then include it in higher
//		if (i == centers.size() - 1)
//			higher = ceilf(thresholds[i + 1]);
//		else
//			higher = floorf(thresholds[i + 1]);

//		std::cout << i << " " << thresholds[i] <<" "<< thresholds[i+1] << std::endl;

		//if (lower > higher)
		//	higher = lower;

//		std::cout << "Lower: " << lower << std::endl;
//		std::cout << "Higher: " << higher << std::endl;

//		num = 0, den = 0;

		//Check the slides for the formulae
//		for (int j = lower; j <= higher; j++) {
////			std::cout << "j=" << j <<std::endl;
//			num += j * pdf.at(j);
//			den += pdf.at(j);
//		}

//		if (den != 0) {
//			new_centers.at(i) = (float) num / den;
//		}
//	}

	return new_centers;
}

int ScalarQuantizer::test_which_interval() {

	int result = 0, interval = -1;

	std::vector<float> thresholds;

	for (int i = 0; i <= 10; i++) {
		thresholds.push_back(3 * i);
	}

	interval = which_interval(thresholds, -1);
	if (interval != 0) {
		result = 1;
		log(logERROR) << "Below-interval value misplaced\n";
	}

	interval = which_interval(thresholds, 0);
	if (interval != 0) {
		result = 1;
		log(logERROR) << "Lower-bound value misplaced\n";
	}

	interval = which_interval(thresholds, 1);
	if (interval != 0) {
		result = 1;
		log(logWARNING) << "Value 1 from beginning of threshold misplaced\n";
	}

	interval = which_interval(thresholds, 2);
	if (interval != 0) {
		result = 1;
		log(logWARNING) << "Value 2 from beginning of threshold misplaced\n";
	}

	interval = which_interval(thresholds, 3);
	if (interval != 1) {
		result = 1;
		log(logWARNING) << "Value 3 misplaced\n";
	}

	interval = which_interval(thresholds, 15);
	if (interval != 5) {
		result = 1;
		log(logWARNING) << "Value 15 misplaced\n";
	}

	interval = which_interval(thresholds, 16);
	if (interval != 5) {
		result = 1;
		log(logWARNING) << "Value 16 misplaced\n";
	}

	interval = which_interval(thresholds, 30);
	if (interval != 10) {
		result = 1;
		log(logWARNING) << "Value 30 from the upper bound of threshold misplaced\n";
	}

	interval = which_interval(thresholds, 42);
	if (interval != 10) {
		result = 1;
		log(logWARNING) << "Value 42 from over the upper bound misplaced\n";
	}

	return result;
}

// True if element found
bool ScalarQuantizer::find(const std::vector<float> vec, const int val) {
	std::vector<float>::const_iterator i;
	i = vec.begin();

	for (i = vec.begin(); i != vec.end(); i++) {
		if (*i == val)
			return true;
	}
	return false;
}

// Tell which interval the key belongs to
// [x] Key to float,
// [ ] Return threshold number
int ScalarQuantizer::which_interval(const std::vector<float> thresholds, const float& value) {

	int i;

	// To check below range
	if (value < thresholds.at(0)) {
		i = 0;
		return i;
	}

	// To check above range
	if (value > thresholds.at(thresholds.size() - 1)) {
		i = thresholds.size() - 1;
		return i;
	}

	if (value > thresholds.at(0) && value < thresholds.at(1)) {
		i = 0;
		return i;
	}

	i = std::distance( thresholds.begin(), lower_bound( thresholds.begin(), thresholds.end(), value ));

	// To correct +1 difference in reported distance
	//   when the `value` falls elsewhere than interval bound.
	if ( find(thresholds, value) == false ) {
		i--;
		return i;
	}

//	i--;

	return i;

//	for (i = 0; i < thresholds.size() - 1; i++) {
//		if ( value > thresholds.at( i ) ) {
//			continue;
//		}
//		else
//			return i;
////			if (i == 0)
////				return i;
////			else
////				return i - 1;
//	}

//	return -1;
}

/**
 * This function gives the quantized value given
 * the key(value).
 */
float ScalarQuantizer::getQuantizedValue(
		const float key,
		const std::vector<float> thresholds,
		const std::vector<float> centers)
{

//	int count;
//	int level, max_val;

	// Initialize to silence compiler warning
//	level = 0;

//	int min_val;
//	min_val = *(thresholds.begin());
//	max_val = *(thresholds.end());

//	count = centers.size();

//	if (key == max_val) {
//		return centers[count - 1];
//	}

	int interval = which_interval(thresholds, key);

	assert(key >= 0);

	if ( interval != -1 )
		return centers[interval];
	else {
		log(logWARNING) << "Key did not fit into any threshold. Key value: " << key << '\n';
		exit(0);
	}
//		if (key >= thresholds[i] && key <= thresholds[i + 1]) {
//			level = i;
//			return centers[level];
//		}
//		if (key >= thresholds[thresholds.size() - 2] && key <= thresholds[thresholds.size() - 1]) {
//			level = i;
//			return centers[level];
//		}

}

// Find the element Greatest In Map
int ScalarQuantizer::gim(const std::map<int, int> m) {

	int max = (*m.begin()).first;

	if ( m.size() == 0 ) {
		log(logERROR) << "Error: Empty map received. Exiting.\n";
		return -1;
	}

	std::map<int, int>::const_iterator it;

	for (it = m.begin(); it != m.end(); it++) {
		if ( (*it).first > max )
			max = (*it).first;
	}

	return max;
}

// Find the element Smallest In Map
int ScalarQuantizer::sim(const std::map<int, int> m) {

	int min = (*m.begin()).first;

	if ( m.size() == 0 ) {
		log(logERROR) << "Error: Empty map received. Exiting.\n";
		return -1;
	}

	std::map<int, int>::const_iterator it;

	for (it = m.begin(); it != m.end(); it++) {
		if ( (*it).first < min )
			min = (*it).first;
	}

	return min;
}

// Divide a threshold from `0` to `maxval` into `count` thresholds
// Out: Vector with threshold boundaries for `count` thresholds; together with the upper boundary of the original threshold.
std::vector<float> ScalarQuantizer::slice(
		const int maxval /* Maximum input value */,
		const int count /* Number of output thresholds */) {

	std::vector<float> thresholds;

	// To keep precision of further division
	// in floating-point precision.
	float maxvalf = maxval,
			countf = count;

	float interval_size;
	interval_size = maxvalf / countf;

	for (int i = 0; i <= count; i++) {
		thresholds.push_back(0 + interval_size * i);
	}

	// To correct for possible numeric errors in
	// end value calculation.
	// -- is to access the last vector value
	// instead of the last iterator.
	thresholds.push_back(maxval);

	return thresholds;
}

// Calculate centers from thresholds.
std::vector<float> ScalarQuantizer::place_centers(const std::vector<float> thresholds) {
	std::vector<float> centers;

	assert(thresholds.size() > 2);

	float center;
	// Position centers in the middle of each interval.
	for (int i = 0; i < thresholds.size() - 1; i++) {
		center = (thresholds[i] + thresholds[i+1]) / 2;
		//if (centers.at(i)
		centers.push_back(center);
	}

	return centers;
}

// hist - data histogram
// count - desired number of output levels
//std::pair<std::vector<float>, std::vector<float>> ScalarQuantizer::initialize(const std::vector<int> data, std::map<int, int> hist, const int count) {
//
//	// size(thresholds) = count + 1
//	std::vector<float> thresholds;
//	// size(centers) = count
//	std::vector<float> centers;
//
////	thresholds.reserve(count + 1);
//
////	centers.reserve(count);
//
//	int min_val, max_val;
//	min_val = (*hist.begin()).first;
//
//	std::map<int, int>::const_iterator it;
//	it = hist.end();
//	it--;
//	max_val = (*it).first;
//
////	std::cout << "min_val: " << min_val << " max val: " << max_val << std::endl;
//
//	// Start and end intervals
////	thresholds[0] = min_val;
////	thresholds[count] = max_val;
//
//	if (b == bin_placement::REGULAR) {
//
//		thresholds = slice(255, count);
//		//centers = place_centers(thresholds);
//
////		float interval_size;
////
////		// Uniform intervals
////		if (count == 0)
////			exit(1);
////		interval_size = (float) (max_val - min_val) / count;
////
////		thresholds.push_back(min_val);
////
////		// Define interval boundaries
////		for (int i = 1; i < count; i++) {
////				thresholds.push_back(i * interval_size);
////		}
////		thresholds.push_back(max_val);
//	}
//	else if (b == bin_placement::RANDOM){
//		srand(0);
//		int r = 0;
//
//		thresholds.push_back(min_val);
//
//		for (int i = 1; i < count - 1; i++) {
//			r = rand() % (max_val + 1);
//			thresholds.push_back(r);
//		}
//
//		thresholds.push_back(max_val);
//
//		std::sort(thresholds.begin(), thresholds.end());
//
//	}
//	else if (b == bin_placement::ADAPTIVE) {
//		std::pair<std::vector<float>, std::vector<float> > p;
//
//		p = adapt(data, hist, count);
//	}
//
//	centers = place_centers(thresholds);
//
////	double center;
////	// Position centers in the middle of each interval.
////	for (int i = 0; i < count; i++) {
////		center = (thresholds[i] + thresholds[i+1]) / 2;
////		//if (centers.at(i)
////		centers.push_back(center);
////	}
//
//	return std::pair<std::vector<float>, std::vector<float>>(thresholds, centers);
//}

bool ScalarQuantizer::found(const std::vector<float>& vec, const int& num) {

	std::vector<float>::const_iterator it;

	it = vec.begin();


	for (; it != vec.end(); it++) {
		if (*it == num)
			return true;
	}

	return false;
}

// Return a map with array value counts
// Suitable only for *integer* input
std::map<int, int> ScalarQuantizer::bin_counts(const std::vector<int> in) {
	std::map<int, int> histogram;

	std::vector<int>::const_iterator it;

	for (it = in.begin(); it != in.end(); it++) {
		if (histogram.count(*it) != 0)
			histogram.at(*it)++;
		else
			histogram.emplace(*it, 1);
	}

	return histogram;
}
//reca
/**
 * Computer the mean square error
 * between the original data and quantized data.
 */
float ScalarQuantizer::getMSE(
		const std::vector<int>& data,
		const std::vector<float>& thresholds,
		const std::vector<float>& centers)
{

//	std::cout << __func__ << std::endl;

	double MSE = 0, diff;

	float QV;

	int val;

	for (size_t i = 0; i < data.size(); i++) {
		val = data[i];
		QV = getQuantizedValue(data[i], thresholds, centers);
		diff = pow((val - QV), 2);
		MSE += diff;
	}

	if (data.size() == 0)
		exit(1);

	MSE /= data.size();

	return MSE;
}

/**
 * This function does the Lloyd Max training given
 * the read data and the number of bits.
 * It sets the quantization levels (T) and the centroids (R) in
 * the global file.
 */

// Map To Vector converter
std::vector<int> ScalarQuantizer::mtv(const std::map<int, int> hist) {
	std::vector<int> pdf;

	// Ensure pdf to be filled with zeros.
	// pdf.resize(hist.size());

	int bins;
//	bins = gim(hist) - sim(hist) + 1;
	bins = gim(hist) + 1;

	pdf.resize(bins + 10, 0); // Ugly workaround
//	pdf.resize(hist.size(), 0);

	for (std::map<int, int>::const_iterator it = hist.begin(); it != hist.end(); it++) {
		pdf[(*it).first] = (*it).second;
	}

	return pdf;
}

std::vector<int> ScalarQuantizer::pdf(const std::vector<int> data) {
// Contains the count of each pixel value.
	// (Probability Density Function)
	std::map<int, int> pdf;
	pdf = bin_counts(data);

	std::vector<int> pdf_v;
	pdf_v = mtv(pdf);

	return pdf_v;
}

/**
 * This function adapts the quantizer parameters
 * to the input data, using the Lloyd-Max algorithm.
 * It outputs a pair of float-type vectors:
 * - Quantization levels (thresholds), and
 * - Centroid positions (centers)
 */
std::pair<std::vector<float>, std::vector<float>> ScalarQuantizer::adapt(const std::vector<int> data, const int count, const std::map<int, int> hist, std::vector<float> thresholds, std::vector<float> centers) {

//	std::cout << __func__ << std::endl;

	float mse = INFINITY, mse_new = 1;

	std::vector<int> pdf_v;

	pdf_v = mtv(hist);

	thresholds = slice(255, count);
	centers = place_centers(thresholds);

	log(logDEBUG) << "Centers:";
	print_pub(centers);

	// Precision of convergence
//	float eps;
//	eps = 1200;

	do {
//		mse = mse_new;

		log(logDEBUG) << "Centers:";
		print_pub(centers);

		centers = recalculate_centers(pdf_v, thresholds, centers);

		log(logDEBUG) << "New centers:";
		print_pub(centers);

		log(logDEBUG) << "Thresholds:";
		print_pub(thresholds);

		thresholds = recalculate_thresholds(thresholds, centers);

		log(logDEBUG) << "New thresholds:";
		print_pub(thresholds);

		mse = mse_new;

		mse_new = getMSE(data, thresholds, centers);

	} while (mse_new > mse);

//	std::cout << "Quantization parameters ready" << std::endl;

//	std::cout << "PDF:\n";
//	print_pub(pdf_v);

	return std::pair<std::vector<float>, std::vector<float>>(thresholds, centers);
}

std::pair<std::vector<float>, std::vector<float>> ScalarQuantizer::randomize_thresholds(const std::vector<int> data, const std::map<int, int> hist, const int count) {
	srand(0);
	int r = 0;

	// size(thresholds) = count + 1
	std::vector<float> thresholds;
	// size(centers) = count
	std::vector<float> centers;

	int min_val, max_val;
	min_val = 0;

	std::map<int, int>::const_iterator it;
	it = hist.end();
	it--;

//	max_val = hist.size() - 1;
	max_val = 255;

	thresholds.push_back(min_val);

	for (int i = 1; i < count - 1; i++) {
		r = rand() % (max_val + 1);
		thresholds.push_back(r);
	}

	thresholds.push_back(max_val);

	std::sort(thresholds.begin(), thresholds.end());

	return std::pair<std::vector<float>, std::vector<float>>(thresholds, centers);
}

//ScalarQuantizer::ScalarQuantizer(const std::vector<int> data, const int count) {
//	std::vector<int> quantized;
//	std::vector<float> thresholds, centers;
//	std::pair<std::vector<float>, std::vector<float>> params;
//
//	params = adapt(data, count);
//
//	thresholds = params.first;
//	centers = params.second;
//
//	std::cout << "Thresholds:" << std::endl;
//	print(thresholds);
//
//	std::cout << "Centers:" << std::endl;
//	print(centers);
//}

template <typename T>
std::string ScalarQuantizer::print( T t )
{
	std::string out;

		for (auto it = t.begin(); it != t.end(); it++) {
				out += *it;
				out += " ";
		}

		out.pop_back();
		out += '\n';

		return out;
}

std::string ScalarQuantizer::print_pub(const std::vector<int> t)
{
	std::stringstream out;

	for (std::vector<int>::const_iterator it = t.begin(); it != t.end(); it++) {
		out << *it << " ";
	}

	out << '\n';

//	out.pop_back();
	out << '\n';

	log(logDEBUG) << out.str();

	return out.str();
}

std::string ScalarQuantizer::print_pub(const std::vector<float> t)
{
	std::stringstream out;

	for (std::vector<float>::const_iterator it = t.begin(); it != t.end(); it++) {
		out << *it << " ";

//			out += *it;
//			out += " ";
	}

//	out << '\n';

//	out.pop_back();
	out << '\n';

	log(logDEBUG) << out.str();

	return out.str();
}

std::vector<int> ScalarQuantizer::analyze_and_quantize(const std::vector<int> data, const int count, std::vector<float>& thresholds, std::vector<float>& centers, bin_placement bp) {

	std::vector<int> out;
	std::vector<int> quantized;

	float quantized_v;

	std::pair<std::vector<float>, std::vector<float>> params;

	switch (bp) {
		case bin_placement::ADAPTIVE: {
			std::map<int, int> hist;

			hist = bin_counts(data);

			params = adapt(data, count, hist, thresholds, centers);

			thresholds = params.first;

			centers = params.second;

			break;
		}
		case bin_placement::REGULAR:

			thresholds = slice(255, count);

			centers = place_centers(thresholds);

			break;

		case bin_placement::RANDOM: {
			std::map<int, int> hist;

			hist = bin_counts(data);

			params = randomize_thresholds(data, hist, count);

			thresholds = params.first;
			// No valid centers contained in `params`.
			centers = place_centers(thresholds);

			break;
		}
		case bin_placement::WEIGHTED: {
			std::map<int, int> hist;

			hist = bin_counts(data);

			thresholds = slice(255, count);

			centers = k_means(mtv(hist), thresholds, centers);

			break;
		}
	}

	int v = 0;

	for(size_t i = 0; i < data.size(); i++) {
		quantized_v = getQuantizedValue(data[i], thresholds, centers);

		v = std::rint(quantized_v);

		out.push_back(v);
	}

	return out;
}

//std::vector<int> ScalarQuantizer::quantize(const std::vector<int> data, const int count) {
//
//	std::vector<int> out;
//	std::vector<int> quantized;
//	std::vector<float> thresholds, centers;
//	std::pair<std::vector<float>, std::vector<float>> params;
//
//	params = adapt(data, count);
//
//	thresholds = params.first;
//	centers = params.second;
//
//	int v = 0;
//
//	for(size_t i = 0; i < data.size(); i++) {
//		v = getQuantizedValue(data[i], thresholds, centers);
//		out.push_back(v);
//	}
//
//	return out;
//}

//std::vector<int> ScalarQuantizer::process(const std::vector<int> data, const int count) {
//
//	std::vector<int> t;
//
//	t = quantize(data, count);
//
////	print(t);
//
//	return t;
//}

std::vector<int> ScalarQuantizer::print_pdf(const std::vector<int> t) {
	std::map<int, int> hist;
	hist = bin_counts(t);
	std::vector<int> pdf;
	pdf = mtv(hist);

	log(logDEBUG) << "PDF:\n";
	print_pub(pdf);

	return pdf;
}

std::vector<int> ScalarQuantizer::process(const std::vector<int> data, const int count, std::vector<float>& thresholds, std::vector<float>& centers, bin_placement bp) {

	std::vector<int> t;

	print_pdf(data);

	t = analyze_and_quantize(data, count, thresholds, centers, bp);

	print_pdf(t);

	return t;
}
