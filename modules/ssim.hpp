/*
 * ssim.hpp
 *
 *  Created on: 22 juil. 2017
 *      Author: patryk
 */

#ifndef MODULES_SSIM_HPP_
#define MODULES_SSIM_HPP_

#include <iostream>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>

class SSIM {
public:
	static double ssim(const cv::Mat& reference, const cv::Mat& evaluated);
private:
	static cv::Scalar mssim(const cv::Mat& i1, const cv::Mat& i2);
};

#endif /* MODULES_SSIM_HPP_ */
