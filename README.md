# Scalar quantization with a Lloyd-Max algorithm implementation

## Example

Original image                   |  Quantized image 
:-------------------------------:|:-------------------------:
![](Debug/t/obrazy/lena.jpg)     |  ![](Debug/t/obrazy/lena_ada_4b.jpg)
8 bits, 222.5 kB zipped          |  4 bits, 60.8 kB zipped

## Idea of quantization

The aim of quantization is to **reduce** the number of **bits** in the depth of the image.

The [Lloyd-Max](https://en.wikipedia.org/wiki/Lloyd%27s_algorithm) algorithm uses a [k-means](https://en.wikipedia.org/wiki/K-means_clustering) approach to maximize the visual similarity of the quantize image to the original.

The idea of operation is shown below.

![Program simplified workflow](doc/idea.png)

## Usage

`./simple_quantizer <filename> <bit_depth> [calibration_algorithm] [verbosity_level]`

Available calibration algorithms:

1. Regular,
2. Random,
3. Weighted (k-means),
4. Doubly-weighted (Lloyd-Max).

## Program text output

A test program execution through `make go` processes the file `lena.pgm` in several different modes, each time printing the output file name with mode indication (quantizer calibration method, output bit depth), and resultant [Structural Similarity Index (SSIM)](https://en.wikipedia.org/wiki/Structural_similarity) in comparison to the input image.

![](doc/operation_capture.png)

## Details

Number of output bits has been set in the code (default = 6).

## Build

The implementation requires a C++17 compiler.

The author uses Clang to build the package, and Eclipse as the default IDE.

Build with the command:
`clang++ -std=c++1y -O0 -g3 -Wall -c -fmessage-length=0`

### Features

- Enables efficient batch processing using *Bash* scripts (*FIFO* compliant),
- Follows the "No news is good news" principle of operation derived from *UNIX*,
- Efficient thanks to precompilation (*C++*), and
- Simple to understand and modify thanks to:
  - Open-source nature and principles, and
  - Highly abstracted implementation (*C++17*).

## Applications of color quantization

- Content-Based Image Recognition systems,
  - QBIC demonstrates that by using quantized color histograms and the quadratic distance images can be effectively compared visually.
